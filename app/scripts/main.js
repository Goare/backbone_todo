$(function(){
  console.log('\'Allo \'Allo!');




  // var AppView = Backbone.View.extend({
  //   el: '.container',
  //   template: _.template("<h3>Hello <%= who %></h3>"),
  //   initialize: function() {
  //     this.render()
  //   },
  //   render: function() {
  //     this.$el.html(this.template({who: 'World!!!!'}));
  //   }
  // });
  // var appView = new AppView();


  window.app = {};
  app.Todo = Backbone.Model.extend({
    defaults: {
      title: '',
      completed: false
    },
    toggle: function() {
      this.set('completed', !this.get('completed'));
    }
  });

  var todo = new app.Todo({title: 'Learn Backbone.js', completed: false});
  todo.get('title');
  todo.get('completed');
  todo.get('created_at');
  todo.set('created_at', Date());
  todo.get('created_at');


  app.TodoList = Backbone.Collection.extend({
    model: app.Todo,
    localStorage: new Backbone.LocalStorage('backbone-todo'),
    completed: function() {
      return this.filter(function(todo){
        return todo.get('completed')
      })
    },
    remaining: function() {
      return this.filter(function(todo){
        return !todo.get('completed')
      })
    }
  });

  app.todoList = new app.TodoList();

  // todoList.create({title: 'Learn Backbone\'s Collection'});
  // var lmodel = new app.Todo({title: 'Learn Models', completed: false});
  // todoList.add(lmodel);
  // todoList.pluck('title');
  // todoList.pluck('completed');
  // JSON.stringify(todoList);


  app.TodoView = Backbone.View.extend({
    tagName: 'li',
    template: _.template($('#item-template').html()),
    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
      this.input = this.$('.edit');
      return this;
    },
    initialize: function() {
      this.model.on('change', this.render, this);
      this.model.on('destroy', this.remove, this);
    },
    events: {
      'dblclick label': 'edit',
      'keypress .edit': 'updateOnEnter',
      'blur .edit': 'close',
      'click .toggle': 'toggleCompleted',
      'click .destroy': 'destroy'
    },
    edit: function() {
      this.$el.addClass('editing');
      this.input.focus();
    },
    close: function() {
      var value = this.input.val().trim();
      if (value) {
        this.model.save({title: value})
      }

      this.$el.removeClass('editing');
    },
    updateOnEnter: function(e) {
      if (e.which == 13) {
        this.close();
      }
    },
    toggleCompleted: function() {
      this.model.toggle();
      if (filter)
        this.$el.toggle();
    },
    destroy: function() {
      this.model.destroy();
    }
  });

  // var view = new app.TodoView({model: todo});



  app.AppView = Backbone.View.extend({
    el: '#todoapp',
    initialize: function() {
      this.input = this.$('#new-todo');
      app.todoList.on('add', this.addOne, this);
      app.todoList.on('reset', this.addAll, this);
      app.todoList.fetch();
    },
    events: {
      'keypress #new-todo': 'createTodoOnEnter'
    },
    createTodoOnEnter: function(e) {
      if (e.which !== 13 || !this.input.val().trim()) {
        return;
      }

      app.todoList.add(this.newAttributes());
      this.input.val('');
    },
    addOne: function(todo) {
      var view = new app.TodoView({model: todo});
      $('#todo-list').append(view.render().el);
    },
    addAll: function() {
      this.$('#todo-list').html('');

      switch(window.filter) {
        case 'pending':
          _.each(app.todoList.remaining(), this.addOne);
          break;
        case 'completed':
          _.each(app.todoList.completed(), this.addOne);
          break;
        default:
          console.log(app.todoList);
          app.todoList.each(this.addOne);
          break;
      }
    },
    newAttributes: function() {
      return {
        title: this.input.val().trim(),
        completed: false
      }
    }
  });





  app.Router = Backbone.Router.extend({
    routes: {
      '*filter': 'setFilter'
    },
    setFilter: function(params) {
      console.log('app.router.params = ' + params);

      if (!params) params = '';

      window.filter = params.trim() || '';
      app.todoList.trigger('reset');
    }
  });

  app.router = new app.Router();
  Backbone.history.start();
  app.appView = new app.AppView();



});
